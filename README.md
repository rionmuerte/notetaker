# notetaker

Simple note keeping app for KDE using offline,
privacy respecting speech to text tools utilizing
[VOSK-API](https://github.com/alphacep/vosk-api)
over [nerd-dictation](https://github.com/ideasman42/nerd-dictation),
with simple KRunner integration.

## Usage

Desktop file provides an interface for the script,
it exposes its options in the settings menu for quick shortcut binding.

Intended use is for the program to be called using shortcuts while working,
to create notes in the background,
without the need for switching to other application.

Every time a note is recorded,
user is prompted with a notification displaying the content of the captured text.
After starting the recording, user can use the same shortcut to stop the recording.
The notification with the captured content can be displayed again
by setting a proper shortcut for it.


Captured content, can also be sent to the KRunner as a query.

### CLI options

`notetaker` - toggles listening, it times out after `default_timeout` seconds,
this is the main program call.

`notetaker save` - saves the latest capture,
corresponds to the `SaveNote` action in desktop entry.

`notetaker show` - displays the latest notification with the note again,
corresponds to the `ShowNote` action in desktop entry.

`notetaker query` - Sends the content of the latest capture to KRunner,
corresponds to the `SendToKrunner` action in desktop entry.

### Configuration

At the first run,
main files and directories will be created in users home directory.
The program loads the config files from `.config` directory
and stores temporary files in `.cache` and `/tmp` locations.
The `config` file is being sourced at the start of the program to determine
temporary file location and target notes file.
Default timeout nerd-dictation uses to determine when to stop listening
can also be modified.

__Paths__

*__Temporary file directory__*

```bash
cache_dir="$HOME/.cache/notetaker"
```

*__Config file directory__*

```bash
conf_dir="$HOME/.config/notetaker"
```
